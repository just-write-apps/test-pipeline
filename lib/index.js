

/**
 * 
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
function add(a, b) {
  return a + b;
}

/**
 * 
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
function subract(a, b) {
  return a - b;
}

module.exports = {
  add, subract
};