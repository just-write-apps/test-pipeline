const { add, subtract, subract } = require("./index");

describe("add", () => {
  it('should add a negative number', () => {
    const result = add(5, -10);

    expect(result).toEqual(-5);
  });
});

describe("subract", () => {
  it('should subract a negative number', () => {
    const result = subract(5, -10);

    expect(result).toEqual(15);
  })
})